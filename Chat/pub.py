
from flask import Flask, jsonify
from google.cloud import pubsub
import os
import json

batch_settings = pubsub.types.BatchSettings(
    max_bytes=1024,  # One kilobyte
    max_latency=1,  # One second
)

publisher = pubsub.PublisherClient(batch_settings)
topic_path = publisher.topic_path('indigo-bedrock-284319', 'chat')


def hello_world(request):
    request_json = request.get_json()
    if (request.args and 'message' in request.args) or (request_json and 'message' in request_json):
        message = request.args.get('message')
        publisher.publish('chat', data=message)
        return jsonify({'result': 'OK'}), 200
    else:
        return f'Hello World!'

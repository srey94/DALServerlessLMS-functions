import os
import time
import traceback

from flask import Response
from google.cloud import pubsub

status = True

messages = []


def callback(message):
    msg = message.data.decode("utf-8")
    print('Received message: {}'.format(msg))
    messages.append(msg)
    message.ack()


def sub_pull():
    subscriber = pubsub.SubscriberClient()
    subscription_path = subscriber.subscription_path(
        'indigo-bedrock-284319', 'messages')
    subscriber.subscribe(subscription_path, callback=callback)
    print('Listening for messages on: {}'.format(subscription_path))
    while status:
        time.sleep(30)


def hello_world(request):
    try:
        request_json = request.get_json()

        def sub_pull():
            subscriber = pubsub.SubscriberClient()
            subscription_path = subscriber.subscription_path(
                'indigo-bedrock-284319', 'messages')
            subscriber.subscribe(subscription_path, callback=callback)
            print('Listening for messages on: {}'.format(subscription_path))
            while status:
                time.sleep(30)
                yield messages[-1]
        return Response(sub_pull(), mimetype="text/event-stream")
    except:
        traceback.print_exc()
    return f'Hello World!'

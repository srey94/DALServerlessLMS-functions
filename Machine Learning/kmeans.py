import pickle
import sys

from google.cloud import storage
from sklearn.cluster import KMeans
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import adjusted_rand_score

bucket_name = 'dal-serverless-lms'
client = storage.Client()
bucket = client.get_bucket(bucket_name)

blob = bucket.get_blob('traindata.txt')
content = blob.download_as_string()
documents = content.split(',')


vectorizer = TfidfVectorizer(stop_words='english')
X = vectorizer.fit_transform(documents)

# print(X)

num_clusters = sys.argv[-1]
model = KMeans(n_clusters=num_clusters, max_iter=100, n_init=1)
model.fit(X)

pickle.dump(model, storage.open(f'/{bucket_name}/model', 'wb'))

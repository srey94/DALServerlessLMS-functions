from oauth2client.client import GoogleCredentials
from googleapiclient import discovery
from googleapiclient import errors
import time
millis = int((time.time() * 1000))

JOB_NAME = 'census_training_' + str(millis)
BUCKET_ID = 'dal-serverless-lms'
JOB_DIR = f'gs://{BUCKET_ID}/scikit_learn_job_dir'
TRAINING_PACKAGE_PATH = 'dal-serverless-lms/project'
MAIN_TRAINER_MODULE = 'census_training.model'
REGION = 'us-east1'
RUNTIME_VERSION = '2.1'
PYTHON_VERSION = '3.7'

project_name = 'indigo-bedrock-284319'
project_id = 'projects/{}'.format(project_name)

training_inputs = {
    'scaleTier': 'BASIC',
    'packageUris': [f'gs://{TRAINING_PACKAGE_PATH}'],
    'pythonModule': MAIN_TRAINER_MODULE,
    'args': ['--arg1', 'value1', '--arg2', 'value2'],
    'region': REGION,
    'jobDir': JOB_DIR,
    'runtimeVersion': RUNTIME_VERSION,
    'pythonVersion': PYTHON_VERSION, }

job_spec = {'jobId': JOB_NAME, 'trainingInput': training_inputs}


credentials = GoogleCredentials.get_application_default()

cloudml = discovery.build('ml', 'v1', credentials=credentials)

request = cloudml.projects().jobs().create(body=job_spec, parent=project_id)

try:
    response = request.execute()
    print(response)
    # Handle a successful request

except Exception as err:
    print(err)
    # logging.error('There was an error creating the training job.'
    #               ' Check the details:')
    # logging.error(err._get_reason())

import json
import traceback

from flask import jsonify
from google.cloud import storage

numClusters = 5
bucketName = 'dal-serverless-lms'

client = storage.Client()
bucket = client.get_bucket(bucketName)


def hello_world(request):
    try:
        request_json = request.get_json()
        wordList = []
        currentCluster = 0
        message = request.args.get('message')
        fileObj = request.files['file']
        message = fileObj.filename
        print(message)
        message = message.split('.')[0]
        loaded_model = pickle.load(storage.open(f'/{bucketName}/model', 'rb'))
        currentCluster = loaded_model.predict(message)[0]
        blob = bucket.blob(f'cluster{currentCluster}/' + message + '.txt')
        blob.upload_from_file(fileObj)
        return jsonify({'word': message, 'cluster': currentCluster, 'wordClusters': wordList})

    except Exception as e:
        traceback.print_exc()
        return f'Error! Check logs'

import pymysql

# 1. Install pymysql to local directory
# pip install -t $PWD pymysql

# 2. (If Using Lambda) Write your code, then zip it all up 
# a) Mac/Linux --> zip -r9 ${PWD}/function.zip 
# b) Windows --> Via Windows Explorer

# Lambda Permissions:
# AWSLambdaVPCAccessExecutionRole

#Configuration Values
endpoint = ''
username = ''
password = ''
database_name = ''

#Connection
connection = pymysql.connect(endpoint, user=username,
    passwd=password, db=database_name)

def lambda_handler():
    cursor = connection.cursor()
    cursor.execute('SELECT email from status')

    rows = cursor.fetchall()
    print(rows)
    
    for row in rows:
        print(row)


a=lambda_handler()
print(a)

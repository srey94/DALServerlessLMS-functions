import boto3
import json
import pymysql

s3_resource = boto3.resource('s3')
s3_client = boto3.client('s3')
db = pymysql.connect("serverless-user-management.cqh7f9ijklg1.us-east-1.rds.amazonaws.com","admin","serverless123","user_managment")


def lambda_handler(event, context):
    cursor = db.cursor()
    req = json.loads(event["body"])
    
    print("request "+str(req))
    ans = req["user_answer"]
    email = req["email"]
    print("answer of the user is "+str(req["user_answer"]))
    cursor.execute("select Answers from user_security where email=%s",(email))
    myResult=cursor.fetchone();
    if(myResult is not None and ans==myResult[0]):
        dbResult = str(myResult[0])
        print("db result "+str(dbResult))
        cursor.execute("INSERT INTO status (email) VALUES(%s)",(email))
        db.commit()
        return json.dumps({'status':True,'message':'User Answer Valid'})
    else:
        return json.dumps({'status':False,'message':'User Answer In-Valid'})
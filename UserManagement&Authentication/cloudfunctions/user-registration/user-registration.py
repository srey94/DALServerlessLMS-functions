import requests
import json
import pymysql

db = pymysql.connect("serverless-user-management.cqh7f9ijklg1.us-east-1.rds.amazonaws.com","admin","serverless123","user_managment")


def hello_world(request):
    cursor = db.cursor()
    udata = request.get_json()
    email = udata['email']
    password = udata['password']
    fname = udata['firstName']
    lname = udata['lastName']
    university = udata['university']
    question = udata['sec_question']
    answer = udata['answer']
    role = udata['role']
    print("data from req question "+question)
    print("Data from request email" +email)
    print("Data from request pwd " +password)
    print("data from req "+fname)
    print("data from request "+lname)
    print("data from request role is "+role)
    cursor.execute("select * from user_detail where email=%s",(email))
    myresult = cursor.fetchone()
    if myresult is None:
        cursor.execute("INSERT INTO user_detail(firstName,university,lastName,email,role) VALUES (%s,%s,%s,%s,%s) ", (fname,university,lname,email,role))
        db.commit()
        qid =cursor.execute("select QuestionID from security_questions where question= %s",(question))
        myqid=cursor.fetchone()
        myquestion = myqid[0]
        print("response "+str(myquestion))
        if myqid is not None:
            myquestion = myqid[0]
            cursor.execute("INSERT INTO user_security(QuestionID,Answers,email) VALUES(%s,%s,%s)",(myquestion,answer,email))
            db.commit()
        firbaseAuthURL = "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyAnqDyjE3qxbEuM3zQQ4GOxC6fiidp5Uug"
        authData={}
        authData['email']=email
        authData['password']=password
        authData['returnSecureToken']=False
        firebaseRequest =requests.post(firbaseAuthURL,data=json.dumps(authData))
        return firebaseRequest.text
            
    else:
        return  json.dumps({'status':False,'message':'User already exist'})

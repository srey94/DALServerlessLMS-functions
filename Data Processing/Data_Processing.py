import os
from flask import Flask, flash, request, redirect, url_for, session, make_response, jsonify
from flask_cors import CORS, cross_origin
import io
from matplotlib.figure import Figure
from google.cloud import storage

import matplotlib.pyplot as plt
from wordcloud import WordCloud, STOPWORDS
import numpy as np
from PIL import Image
import nltk
from nltk import ne_chunk, pos_tag
from nltk.tree import Tree

nltk.download('stopwords')
nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')
nltk.download('maxent_ne_chunker')
nltk.download('words')
from nltk.tokenize import word_tokenize
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
import base64

UPLOAD_FOLDER = 'D:/ServerlessLab/flaskupload/backend'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
CORS(app)


@app.route('/upload', methods=['POST'])
def fileUpload():

    uploaded_file = request.files.get('file')

    if not uploaded_file:
        return 'No file uploaded.', 400

    # Create a Cloud Storage client.
    gcs = storage.Client.from_service_account_json("eminent-glider-278618-12d5b9bca2a6.json")

    # Get the bucket that the file will be uploaded to.
    BUCKET_NAME = 'file_storage_bucket12876'
    bucket = gcs.get_bucket(BUCKET_NAME)

    # Create a new blob and upload the file's content.
    blob = bucket.blob(uploaded_file.filename)

    blob.upload_from_string(
        uploaded_file.read(),
        content_type=uploaded_file.content_type
    )

    return "Success"


@app.route("/try")
def tryd():
    storage_client = storage.Client.from_service_account_json("eminent-glider-278618-12d5b9bca2a6.json")

    BUCKET_NAME = 'file_storage_bucket12876'

    bucket = storage_client.get_bucket(BUCKET_NAME)
    filename = request.args.get('filename')

    filenames = list(bucket.list_blobs(prefix=''))
    for name in filenames:
        print(name.name)

    blop = bucket.blob(filename)
    data = blop.download_as_string()

    print(data)

    text_tokens = word_tokenize(str(data.decode("utf-8")))
    tokens_without_sw = [word for word in text_tokens if not word in stopwords.words()]
    tokenizer = RegexpTokenizer(r'\w+')
    result = tokenizer.tokenize(' '.join(tokens_without_sw))
    inp = " ".join(result)
    chunked = ne_chunk(pos_tag(word_tokenize(inp)))
    named_entities = []
    for i in chunked:
        if type(i) == Tree:
            wrd = " ".join([token for token, pos in i.leaves()])
            named_entities.append(wrd)

    print(named_entities)

    custom_mask = np.array(Image.open("mask.png"))
    cloud = WordCloud(background_color="white", mask=custom_mask, max_words=200, stopwords=set(STOPWORDS)).generate(
        " ".join(named_entities))
    # cloud.to_file("wordCloud.png")
    fig = Figure()
    plt.imshow(cloud, interpolation='bilinear')
    plt.axis("off")
    img = io.BytesIO()
    plt.savefig(img, format='png')
    img.seek(0)
    plot_url = base64.b64encode(img.getvalue()).decode()
    print(jsonify(plot_url))
    return (plot_url)


if __name__ == "__main__":
    app.secret_key = os.urandom(24)
    app.run(debug=True, host="0.0.0.0", use_reloader=False, port=8000)


import json
import boto3

s3 = boto3.client('s3')
comprehend = boto3.client(service_name='comprehend')


def lambda_handler(event, context):
    dict = {}
    s3 = boto3.client("s3")
    if (event):
        file_records = event["Records"][0]
        file_name = str(file_records["s3"]["object"]["key"])
        file_object = s3.get_object(
            Bucket="dalserverlesslms", Key=file_name)
        content = str(file_object["Body"].read().decode("utf-8"))

        object_list = json.loads(content)
        messages = []
        for object in object_list:
            messages.append(object['message'])

        resultList = comprehend.batch_detect_sentiment(
            TextList=messages, LanguageCode='en')["ResultList"]
        for i in range(len(messages)):
            resultList[i]['message'] = messages[i]
        print(resultList)
        s3.put_object(Body=json.dumps(resultList),
                      Bucket="dalserverlesslms-sentiment", Key=file_name)

    return {
        'statusCode': 200,
        'body': json.dumps('Lambda Successful!')
    }

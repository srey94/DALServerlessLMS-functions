# CSCI 5410 Project

DalServerlessLMS lets authenticated users perform various operations like data processing, machine learning, sentiment analysis, chat online. This is the repository for all the cloud and lambda functions

-   _Date Created_: 07 Jul 2020
-   _Last Modification Date_: 06 Aug 2020

## Authors

-   [Avinash Gazula](av530575@dal.ca)
-   [Krupa Patel](Krupa.Patel@dal.ca)
-   [Mayank Bagla](Mak.B@dal.ca)
-   [Sreyas Naaraayanan Ramanathan](srey94@dal.ca)

### Built With

-   [Flask](https://flask.palletsprojects.com/en/1.1.x/)

## Deployment

Functions are based on GCP cloud functions and AWS lambda functions

### Github Link for client

https://github.com/avinashgazula/DALServerlessLMS-client

### Github Link for Serverless functions

https://github.com/avinashgazula/DALServerlessLMS-functions
